package hw_29;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpleMathLibraryTest {

    private final SimpleMathLibrary mathLibrary = new SimpleMathLibrary();


    @Test
    public void TestAddMethod() {
        double a = mathLibrary.add(1, 1);

        Assertions.assertEquals(3, a); // test for failing
        if (a == 2) {
            System.out.println("OK");
        } else {
            System.out.println("NOK");
        }
    }

    @Test
    public void TestMinusMethod() {
        double a = mathLibrary.minus(4, 2);

        Assertions.assertEquals(2, a);
        if (a == 2) {
            System.out.println("OK");
        } else {
            System.out.println("NOK");
        }
    }
}