package hw_29;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.stream.Stream;

public class ComplicatedMathLibraryTest {

    private final ComplicatedMathLibrary mathLibrary = new ComplicatedMathLibrary();

    @ParameterizedTest
    @MethodSource("dataForCutArrayTest")
    public void testCutArrayMethod(int[] arrInput, int[] arrExpected) {

        Assertions.assertArrayEquals(arrExpected, mathLibrary.cutArray(arrInput));
    }

    @Test
    public void testCutArrayMethodException() {

        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> mathLibrary.cutArray(new int[]{1, 3, 5}));

        Assertions.assertEquals("Digit was no found!", exception.getMessage());
    }

    static Stream<Arguments> dataForCutArrayTest() {

        return Stream.of(
                Arguments.of(new int[]{1,2,4,4,2,3,4,1,7}, new int[]{4,2,3,4,1,7}),
                Arguments.of(new int[]{4,2,3,4,1,7}, new int[]{2,3,4,1,7}),
                Arguments.of(new int[]{1,7,4}, new int[]{})
        );
    }


    @ParameterizedTest
    @MethodSource("dataForCheckArrayTest")
    public void testCheckArrayMethod(int[] arrInput, boolean expected) {
        Assertions.assertEquals(expected, mathLibrary.checkArray(arrInput));
    }

    static Stream<Arguments> dataForCheckArrayTest() {

        return Stream.of(
                Arguments.of(new int[]{1,1,1,4,4,1,4,4}, true),
                Arguments.of(new int[]{1,1,1,1,1,1}, false),
                Arguments.of(new int[]{4,4,4,4}, false),
                Arguments.of(new int[]{1,4,4,1,1,4,3}, false)
        );
    }

}