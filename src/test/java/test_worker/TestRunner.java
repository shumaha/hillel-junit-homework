package test_worker;

import hw_29.*;
import org.junit.platform.engine.DiscoverySelector;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.io.*;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class TestRunner {

    public static void main(String[] args) {
        // Запуск тестів по массиву ім'я класу (рядкові значення)
//        runTests("hw_29.SimpleMathLibraryTest", "hw_29.ComplicatedMathLibraryTest");
//        runTests("hw_29.SimpleMathLibraryTest");

        // Запуск тестів по массиву класів
//        runTests(ComplicatedMathLibraryTest.class);
//        runTests(SimpleMathLibraryTest.class, ComplicatedMathLibraryTest.class);


        // Запуск тестів по назві пакета
        runTestsByPackage("hw_29");
    }

    public static void runTests(String... classNames) {
        DiscoverySelector[] selectors = new DiscoverySelector[classNames.length];

        for (int i = 0; i < classNames.length; i++) {
            selectors[i] = selectClass(classNames[i]);
        }

        runTests(selectors);
    }

    public static void runTests(Class<?> ... classes) {
        DiscoverySelector[] selectors = new DiscoverySelector[classes.length];

        for (int i = 0; i < classes.length; i++) {
            selectors[i] = selectClass(classes[i]);
        }

        runTests(selectors);
    }


    public static void runTestsByPackage(String packageName) {

        runTests(selectPackage(packageName));
    }

    private static void runTests(DiscoverySelector... selectors) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectors)
                .build();

        Launcher launcher = LauncherFactory.create();

        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);

        TestExecutionSummary summary = listener.getSummary();

        // print to console
        summary.printTo(new PrintWriter(System.out));

        // print to file
        try (FileWriter fileWriter = new FileWriter("src/test/resources/file.txt", false)) {
            summary.printTo(new PrintWriter(fileWriter));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
