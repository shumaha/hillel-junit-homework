package test_worker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.nonNull;

public class TestResultParser {

    public static void main(String[] args) {

        String location = "src/test/resources/file.txt";
        TestResultParser resultParser = new TestResultParser();

        // парсинг файлу по дорозі (рядкове значення)
         System.out.println(resultParser.parse(location));

        // парсинг файлу по дорозі (тип даних File)
        /*File file = new File(location);
        System.out.println(resultParser.parse(file));*/

        // парсинг файлу по дорозі (тип даних Path)
        /*Path path = Paths.get(location);
        System.out.println(resultParser.parse(path));*/
    }

    public TestResult parse(String filePath) {
        File file = new File(filePath);

        return parse(file);
    }

    public TestResult parse(File file) {
        String content = readFile(file);

        try {
            int totalTestCount = getValue("\\[\\s+\\d+ tests found\\s+]", content);
            int successfulTestCount = getValue("\\[\\s+\\d+ tests successful\\s+]", content);
            int failedTestCount = getValue("\\[\\s+\\d+ tests failed\\s+]", content);
            int totalTestRunTime = getValue("Test run finished after \\d+ ms", content);

             return new TestResult(totalTestCount, successfulTestCount, failedTestCount, totalTestRunTime);

        } catch (NoParsedParameterExceptions e) {
            throw new RuntimeException("One of parameters was no finded! " + e.getMessage());
        }
    }

    public TestResult parse(Path pathfile) {

        File file = pathfile.toFile();

        return parse(file);
    }

    private String readFile(File file) {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;

            while ( (line = br.readLine()) != null) {
                sb.append(line).append(System.lineSeparator());
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return sb.toString();
    }

    private int getValue(String regexp, String content) throws NoParsedParameterExceptions {

        try {
            String stringWithValue = doRegexp(regexp, content);

            if (nonNull(stringWithValue)) {
                stringWithValue = doRegexp("\\d+", stringWithValue);

                if (nonNull(stringWithValue))
                    return Integer.parseInt(stringWithValue);

            }
        } catch (NoParsedParameterExceptions e) {
            throw new NoParsedParameterExceptions("Regexp \""+regexp+"\" was failed!");
        }

        return 0;
    }

    private String doRegexp(String regexp, String content) throws NoParsedParameterExceptions {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(content);

        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new NoParsedParameterExceptions();
        }
    }
}
