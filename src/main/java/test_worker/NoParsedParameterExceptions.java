package test_worker;

public class NoParsedParameterExceptions extends Exception {

    public NoParsedParameterExceptions() {
        super();
    }
    public NoParsedParameterExceptions(String message) {
        super(message);
    }


}
