package test_worker;

public record TestResult(int totalTestCount, int successfulTestCount, int failedTestCount, long totalTestRunTime) {

    @Override
    public String toString() {
        return "TestResult{" +
                "totalTestCount=" + totalTestCount +
                ", successfulTestCount=" + successfulTestCount +
                ", failedTestCount=" + failedTestCount +
                ", totalTestRunTime=" + totalTestRunTime + " ms" +
                '}';
    }
}
