package hw_29;

public class ComplicatedMathLibrary {


    public int[] cutArray(int[] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] == 4) {
                int[] outArray = new int[inputArray.length-i-1];
                System.arraycopy(inputArray, i+1, outArray, 0, outArray.length);

                return outArray;
            }
        }

        throw new RuntimeException("Digit was no found!");
    }

    public boolean checkArray(int[] inputArray) {
        boolean hasOne = false;
        boolean hasFour = false;

        for (int i : inputArray) {
            if (i == 4) {
                hasFour = true;
            }

            if (i == 1) {
                hasOne = true;
            }

            if (i != 1 && i != 4) {
                return false;
            }
        }

        return hasOne & hasFour;
    }
}
